import { Message, RichEmbed } from "discord.js";
import { IMan } from "../interfaces";

export default class Command {
    /**
     * Envoie un message privé à l'author et suprimme la commande
     * @param  {string | RichEmbed} reply
     * @param  {Message} message 
     * @return {Promise<any>}
     */
    protected async replyDM (reply: string | RichEmbed, message: Message): Promise<any> {
        try {
            let channel = await message.author.createDM()
            await channel.send(reply)
        } catch (e) {
            await message.reply(reply)
        }
        if (message.channel.type !== 'dm') {
            await message.delete()
        }
        return
    }

    /**
     * Renvoie le manuel de la commande à l'utilisateur.
     * @param  {IMan} option 
     * @param  {Message} message
     * @return {Promise<any>}
     */
    protected async man(option: IMan, message: Message): Promise<any>{
        return await this.replyDM(new RichEmbed({
            title: option.title, 
            description: option.description,
            color: option.color,
            author: option.author,
            footer: option.footer
        }), message);
    }

    /**
     * Envoie un message d'information à l'utilisateur.
     * @param  {string}       information 
     * @param  {Message}      message     
     * @return {Promise<any>} 
     * @TODO : Faire le flash message d'information            
     */
    protected async info(information: string, message: Message): Promise<any>{
        return await this.replyDM(new RichEmbed({}), message);
    }
    
    /**
     * Envoie un message d'erreur à l'utilisateur.
     * @param  {string}       error   
     * @param  {Message}      message 
     * @return {Promise<any>}         
     * @TODO : Faire le flash message d'erreur            
     */
    protected async error(error: string, message: Message): Promise<any>{
        return await this.replyDM(new RichEmbed({}), message);
    }
}