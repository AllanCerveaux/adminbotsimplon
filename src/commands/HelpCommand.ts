import {Message, RichEmbed} from "discord.js";
import { ICommand } from "../interfaces";
import Command from './Command'

interface ICommandList {
    [name: string]: {
        description: string
        admin: boolean
    }
}

export default class HelpCommand extends Command implements ICommand 
{
    public name: string = "help";
    public description: string = 'Toutes les commandes disponible.';
    public active: boolean = true;
    public admin: boolean = false;
    private commands: ICommand[] = [];

    public constructor(commands: ICommand[]) {
        super();
        this.commands = commands;
    }

    public async run(message: Message, args: String[]){
        let commands: ICommandList = this.commands.reduce((acc: ICommandList, command) => {
            acc[command.name] = {
                description : command.description,
                admin: command.admin
            };
            return acc;
        }, {});

        let commandsName = Object.keys(commands).sort();
        let simplonRole = message.guild.roles.find("name", "Simplon").id;
        let helpAdmin = commandsName.map(name => `**${process.env.COMMAND_NAME}${name}**: ${commands[name].description}`).join('\n')
        let helpUser = commandsName.filter(name => !commands[name].admin).map(name => `**${process.env.COMMAND_NAME}${name}**: ${commands[name].description}`).join('\n')

        let embed = new RichEmbed({
            title: this.description,
            description: `${message.member.roles.has(simplonRole) ? helpAdmin : helpUser}`,
            color: 0x2196f3,
            footer: {
                text: "For more information contact @Callan"
            }
        });

        await this.replyDM(embed, message);
    }

}