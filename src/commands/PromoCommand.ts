import Command from "./Command";
import {ICommand, IMan} from "../interfaces";
import {Message, GuildChannel, CategoryChannel, Guild, Role, RichEmbed, PermissionResolvable} from "discord.js";

interface Promo {
	readonly region: string
	readonly referencial: string
	readonly id: number
}

interface DefaultChannel {
	name: string
	topic: string
}

export default class PromoCommand extends Command implements ICommand
{
	public name: string = "promo";
	public description: string = "Créer une nouvelle promotion et génère un lien d'invitation.";
	public admin: boolean = true;
	public active: boolean = true;
	public manual: IMan = {
		title: "Manuel pour la commande Promo",
		description: `
				**Description**: Créer des channels pour une promotion avec un code d'invitation.
				**Usage**: ${process.env.COMMAND_NAME}promo [option] [arguments]
				**Example**: ${process.env.COMMAND_NAME} promo create Toulouse DWWM 1
				**Options** :
						- create [promo_name] [promo_referentiel]	[promo_number]	| *Créer une promotion avec différents channels*
						- delete [promo_name] [promo_referentiel]	[promo_number]  | *Supprime une promo définitivement, si vous avez des doûtes ne pas lancer cette commande*
		`,
		color: 0x2196f3,
    footer: {
        text : "Si vous détecter un problème avec le bot, laisser une issue : https://gitlab.com/AllanCerveaux/adminbotsimplon/issues"
    },
    author: {
        name: "Callan",
        icon: "https://cdn.discordapp.com/avatars/445485887724322836/a319bde3a953f97a9bd4efa8f8050968.png?size=128",
        url: "https://github.com/AllanCerveaux"
    }
	}
	private channels: DefaultChannel[] = [{name:"général", topic:"Salon pour passer les informations importante ! #nospam"}, {name:"activités", topic:"Partage des activités, entraide..."}, {name:"veilles", topic:"Vos liens utiles pour des technos ou autres"}, {name: "détentes", topic:"Un petit salon pour échanger sur d'autres sujets (toujours dans le respect de l'esprit Simplon, bien entendu)."}, {name: "musique", topic:"Partage ta zik du moment avec tout le monde !"}, {name: "titre-pro", topic:"Ici on envoie tous les outils/infos nécessaires à la certification"}, {name:"insertion-pro", topic:"Ici on envoie tous les outils/infos nécessaires à l'insertion pro"}];
	private basic_perm: PermissionResolvable = ["VIEW_CHANNEL", "READ_MESSAGES", "SEND_MESSAGES", "READ_MESSAGES", "READ_MESSAGE_HISTORY", "ADD_REACTIONS", "CONNECT", "SPEAK", "EMBED_LINKS"];

	public async run(message: Message, args: string[]){
    if (args.length < 1) return await this.commandError(message, args[0]);

		if(args[0] === "man"){
			return await this.man(this.manual, message);
		}else if(args[0] === "create"){
			if(args.length < 4)	return await this.commandError(message, args[0]);
			return await this.makePromo(message, {region: args[1], referencial: args[2], id: parseInt(args[3])});
		}else if(args[0] === "delete"){
			if(args.length < 4)	return await this.commandError(message, args[0]);
			return await this.deletePromo(message, {region: args[1], referencial: args[2], id: parseInt(args[3])});
		}else{
			return await this.commandError(message, args[0]);
		}
	}

	/**
	 * Renvoie une erreur si la command est imcomplète ou incorrecte.
	 * @param {Message} message
	 * @param {string}  command
	 */
  private async commandError(message: Message, command: string){
      switch (command) {
          case undefined:
              this.man(this.manual, message);
          break;
          case "create":
              var embed: RichEmbed = new RichEmbed({
                  title: `Erreur sur la commande ${process.env.COMMAND_NAME}${this.name} ${command}`,
                  description: `
                  Il manque un ou plusieur arguments **${process.env.COMMAND_NAME}${this.name} ${command}** !
                  Essayer **${process.env.COMMAND_NAME}${this.name} ${command}** [promo_name] [promo_referentiel]	[promo_number] ou faite **'${process.env.COMMAND_NAME}${this.name} man'** pour plus informations !
                  `,
                  color: 0xFF0000
              });
              return await this.replyDM(embed, message);
              break;
          case 'delete':
              var embed: RichEmbed = new RichEmbed({
                  title: `Erreur sur la commande ${process.env.COMMAND_NAME}${this.name} ${command}`,
                  description: `
                  Il manque un ou plusieur arguments **${process.env.COMMAND_NAME}${this.name} ${command} **!
                  Essayer **${process.env.COMMAND_NAME}${this.name} ${command}** [promo_name] [promo_referentiel]	[promo_number] ou faite **'${process.env.COMMAND_NAME}${this.name} man'** pour plus informations !
                  `,
                  color: 0xFF0000
              });
              return await this.replyDM(embed, message);
              break;
          default:
          var embed: RichEmbed = new RichEmbed({
              title: `L'argument ${command} n'existe pas`,
              description: `
              La commande **${process.env.COMMAND_NAME}${this.name}** ne possède pas l'argument **${command}** ou faite **'${process.env.COMMAND_NAME}${this.name} man'** pour plus informations !
              `,
              color: 0xFF0000
          });
          return await this.replyDM(embed, message);
          break;
      }
  }

	/**
	 * Créer un nouvelle promotion + liens d'invitation.
	 * @param {module:discord.js.Message} message
	 * @param {Promo} 									  promo
	 */
	private async makePromo(message: Message, promo: Promo){
		return await this.createCategory(message, message.guild, promo);
	}

	/**
	 * Créer un nouvelle catégory si elle n'existe pas déjà
	 * @param {module:discord.js.Message} message
	 * @param {module:discord.js.Guild}   server
	 * @param {Promo} 									  promo
	 */
	private async createCategory(message: Message, server: Guild, promo: Promo){
		let promoName: string = `Simplon ${promo.region} ${promo.referencial} ${promo.id}`
		if(!server.channels.find('name', promoName)){
			return await server.createChannel(promoName, 'category')
			.then((channel: CategoryChannel) => (
	       this.overwriteEveryonePermission(server, channel),
	       this.createPromoRole(server, message, channel, promo),
	       this.createPromoChannel(message, server, channel, promo)
       )
			)
			.catch(err => console.error);
		}
		return this.replyDM(`La promotion ${promoName} existe déjà !`, message);
	}

	/**
	 * Définit les permission du role @everyone sur un channel
	 * @param {module:discord.js.Guild} 	     server
	 * @param {module:discord.js.GuildChannel} channel
	 */
	private async overwriteEveryonePermission(server: Guild, channel: GuildChannel){
		await channel.overwritePermissions(server.roles.find('name', '@everyone'), {
			'ADD_REACTIONS': false,
			'READ_MESSAGES': false,                  'SEND_MESSAGES': false,
			'SEND_TTS_MESSAGES': false,              'MANAGE_MESSAGES': false,
			'EMBED_LINKS': false,                    'ATTACH_FILES': false,
			'READ_MESSAGE_HISTORY': false,           'MENTION_EVERYONE': false,
			'EXTERNAL_EMOJIS': false,                'CONNECT': false,
			'SPEAK': false
		});
	}

	/**
	 * Créer le role de la nouvelle promotion
	 * @param {module:discord.js.Guild} 			 server
	 * @param {module:discord.js.GuildChannel} channel
	 * @param {Promo} promo
	 */
	private async createPromoRole(server: Guild, message:Message, channel: GuildChannel, promo: Promo){
		let rolePromo: string = `${promo.region} ${promo.referencial} #${promo.id}`;
		if(!server.roles.find('name', rolePromo)){
			await server.createRole({
				name: rolePromo,
				color: '#'+(Math.random()*0xFFFFFF<<0).toString(16),
				permissions: this.basic_perm,
				mentionable: true,
				hoist: true
			})
			.then((role: Role) => {
				server.members.find('displayName', message.author.username).addRole(role);
				this.overwritePromoPermission(server, channel, role.name)
			})
			.catch(err => console.error);
		}
	}

	/**
	 * Définit les permission pour le role pour la promo
	 * @param {module:discord.js.Guild} 			 server
	 * @param {module:discord.js.GuildChannel} channel
	 * @param {string} role
	 */
	private async overwritePromoPermission(server: Guild, channel: GuildChannel, role: string){
		await channel.overwritePermissions(server.roles.find('name', role), {
			'CREATE_INSTANT_INVITE' : false,        'ADD_REACTIONS': true,
			'READ_MESSAGES': true,                  'SEND_MESSAGES': true,
			'SEND_TTS_MESSAGES': true,              'MANAGE_MESSAGES': true,
			'EMBED_LINKS': true,                    'ATTACH_FILES': true,
			'READ_MESSAGE_HISTORY': true,           'MENTION_EVERYONE': true,
			'EXTERNAL_EMOJIS': true,                'CONNECT': true,
			'SPEAK': true
		});
	}

	/**
	 * Créer les channels pour la nouvelle promo
	 * @param {module:discord.js.Message} 			  message
	 * @param {module:discord.js.Guild} 					server
	 * @param {module:discord.js.CategoryChannel} parent
	 * @param {Promo} promo
	 */
	private async createPromoChannel(message: Message, server: Guild, parent: CategoryChannel, promo: Promo){
		this.channels.forEach(channel => {
			server.createChannel(channel.name, 'text')
			.then(child => {
				child.setParent(parent.id)
				.then(() => (child.setTopic(channel.topic),
				             this.overwriteEveryonePermission(server, child),
				             this.overwritePromoPermission(server, child, `${promo.region} ${promo.referencial} #${promo.id}`),
				             child.name === "général" ? this.createPromoInstantInvite(message, parent) : null)
				)
				.catch(err => console.error);
			})
			.catch(err => console.error);
		})
	}

	/**
	 * Créer l'invitation pour la promo envoie un message à l'auteur
	 * @param {module:discord.js.Message}				  message
	 * @param {module:discord.js.CategoryChannel} channel
	 */
	private async createPromoInstantInvite(message: Message, channel: CategoryChannel){
		let invitChannel = channel.children.find("name", "général");
		await invitChannel.createInvite({
			temporary: true,
			maxAge : 0,
			maxUses: 16,
			unique: true
		})
		.then((invite: any) => this.replyDM(`Vous pouvez inviter vos aprennant·e·s avec ce liens : https://discord.gg/${invite.code}`, message))
		.catch((err: any) => console.error);
	}

	/**
	 * Supprime une promo (channels, category et role)
	 * @param {module:discord.js.Message} message
	 * @param {Promo}   								  promo
	 */
	private async deletePromo(message: Message, promo: Promo){
		//@ts-ignore
		const promoCategory: CategoryChannel  = message.guild.channels.find('name', `Simplon ${promo.region} ${promo.referencial} ${promo.id}`);
		const promoRole : Role = message.guild.roles.find('name', `${promo.region} ${promo.referencial} #${promo.id}`);
		promoCategory.children.forEach((channel: GuildChannel) => channel.delete());
		promoRole.delete();
		promoCategory.delete()
		  .then(() => this.replyDM(`Promo Simplon **${promo.region} ${promo.referencial} ${promo.id}** a été supprimer !`, message));
	}

}
