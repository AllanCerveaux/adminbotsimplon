import Command from "./Command";
import {ICommand, IMan} from "../interfaces";
import {Message, GuildChannel, Channel, RichEmbed, ChannelData} from "discord.js";

interface IChan {
	readonly name: string
	readonly type: string
}

export default class ChannelCommand extends Command implements ICommand
{
	public name: string = "channel";
	public description: string = "Gestion des channels";
	public admin: boolean = true;
	public active: boolean = true;
	public manual: IMan = {
		title: "Manuel pour la commande Channel",
		description: `
				**Description**: Créer un channel (texte|voix|categorie).
				**Usage**: ${process.env.COMMAND_NAME}channel [option] [arguments]
				**Example**: ${process.env.COMMAND_NAME}channel create Toulouse voice
				**Options** :
						- create [name]	[type]					| Créer un channel. les différent type de channel : "text | voice | category"
						- delete [name]  								| Supprime une channel définitivement, si vous avez des doûtes ne pas lancer cette commande
		`,
		color: 0x2196f3,
    footer: {
        text : "Si vous détecter un problème avec le bot, laisser une issue : https://gitlab.com/AllanCerveaux/adminbotsimplon/issues"
    },
    author: {
        name: "Callan",
        icon: "https://cdn.discordapp.com/avatars/445485887724322836/a319bde3a953f97a9bd4efa8f8050968.png?size=128",
        url: "https://github.com/AllanCerveaux"
    }
	}

	public async run(message: Message, args: string[]){
		if (args.length < 1) return await this.commandError(message, args[0]);

		if(args[0] === "man"){
			return await this.man(this.manual, message);
		}else if(args[0] === "create"){
      if(!args[2]) return await this.commandError(message, args[0]);
			return await this.makeChannel(message, {name: args[1], type: args[2]});
		}else if(args[0] === "delete"){
			return await this.deleteChannel(message, args[1]);
		}else{
			return await this.commandError(message, args[0]);
		}
	}

  /**
   * Renvoie une erreur si la command est imcomplète ou incorrecte.
   * @param {Message} message
   * @param {string}  command
   */
  private async commandError(message: Message, command: string){
    switch (command) {
      case undefined:
          this.man(this.manual, message);
      break;
      case "create":
          var embed: RichEmbed = new RichEmbed({
              title: `Erreur sur la commande ${process.env.COMMAND_NAME}${this.name} ${command}`,
              description: `
              Il manque un ou plusieur arguments ${process.env.COMMAND_NAME}${this.name} ${command} !
              Essayer ${process.env.COMMAND_NAME}${this.name} ${command} [name]	[type] ou faite '${process.env.COMMAND_NAME}${this.name} man' pour plus informations !
              `,
              color: 0xFF0000
          });
          return await this.replyDM(embed, message);
          break;
      case 'delete':
          var embed: RichEmbed = new RichEmbed({
              title: `Erreur sur la commande ${process.env.COMMAND_NAME}${this.name} ${command}`,
              description: `
              Il manque un ou plusieur arguments ${process.env.COMMAND_NAME}${this.name} ${command} !
              Essayer ${process.env.COMMAND_NAME}${this.name} ${command} [name] ou faite '${process.env.COMMAND_NAME}${this.name} man' pour plus informations !
              `,
              color: 0xFF0000
          });
          return await this.replyDM(embed, message);
          break;
      default:
      var embed: RichEmbed = new RichEmbed({
          title: `L'argument ${command} n'existe pas`,
          description: `
          La commande ${process.env.COMMAND_NAME}${this.name} ne possède pas l'argument ${command} ou faite '${process.env.COMMAND_NAME}${this.name} man' pour plus informations !
          `,
          color: 0xFF0000
      });
      return await this.replyDM(embed, message);
      break;
    }
  }

	/**
	 * Créer un channel ou une catégorie
	 * @param {module: discord.js.Message} message
	 * @param {IChan}                      option
	 */
	private async makeChannel(message: Message, option: IChan){
		let server = message.guild;
    if(server.channels.find("name", option.name)) return await this.replyDM('Ce channel éxiste déjà !', message);

		server.createChannel(option.name, option.type);
		return await this.replyDM(`Vous avez créer le **channel ${option.type} ${option.name}** !`, message);
	}

	/**
	 * Supprime un channel
	 * @param {module: discord.js.Message} message
	 * @param {string}                     channel
	 */
	private async deleteChannel(message: Message, channel: string){
		let chan = message.guild.channels.find("name", channel);
		if(!chan){
			return await this.replyDM(`Le channel **${channel}** n'éxiste pas !`, message);
		}else {
			chan.delete();
			return await this.replyDM(`Le channel **${channel}** a été suppimé !`, message);
		}
	}
}
