import Command from "./Command";
import {Message, RichEmbed, RoleData, PermissionResolvable} from "discord.js";
import {ICommand, IMan} from "../interfaces";



export default class RoleCommand extends Command implements ICommand
{
  public name: string = "role";
  public description: string = "Gestion des rôles";
  public admin: boolean = true;
  public active: boolean = true;
  public basic_perm: PermissionResolvable = ["VIEW_CHANNEL", "READ_MESSAGES", "SEND_MESSAGES", "READ_MESSAGES", "READ_MESSAGE_HISTORY", "ADD_REACTIONS", "CONNECT", "SPEAK", "EMBED_LINKS"];
  public manual: IMan = {
    title: "Manuel pour la commande  Role",
    description: `
    **Description**: Créer un role avec des permissions basic ${this.basic_perm.join(',')}
    **Usage**: ${process.env.COMMAND_NAME}role [option] [...arguments]
    **Example**: ${process.env.COMMAND_NAME}role create Toulouse #000FFF
    **Options** :
    - create [name] [color]  | *Créer un role avec des permissions basique, les couleurs hexa sont pris en charge*
    - delete [name]                          | *supprime un role.*
    - list                                   | *list les roles existant.*
    `,
    color: 0x2196f3,
    footer: {
      text : "Si vous détecter un problème avec le bot, laisser une issue : https://gitlab.com/AllanCerveaux/adminbotsimplon/issues"
    },
    author: {
      name: "Callan",
      icon: "https://cdn.discordapp.com/avatars/445485887724322836/a319bde3a953f97a9bd4efa8f8050968.png?size=128",
      url: "https://github.com/AllanCerveaux"
    }
  }

  public async run(message: Message, args: string[]){
    if (args.length < 1) return await this.commandError(message, args[0]);

    if(args[0] === "man"){
      return await this.man(this.manual, message);
    }
    else if(args[0] === "create") {
      return await this.createRole({name: args[1] ? args[1] : this.commandError(message, args[0]), color: args[2], permissions: this.basic_perm}, message)
    }else if(args[0] === "delete"){
      return await this.removeRole(args[1] ? args[1] : this.commandError(message, args[0]), message)
    }else if(args[0] === "list"){
      let embed = new RichEmbed({
        title : "All role available on this server",
        description: message.guild.roles.map(role => `- **${role.name}**`).join("\n"),
        color: 0xE90004
      });

      return await this.replyDM(embed, message);
    }else {
      return await this.commandError(message, args[0])
    }
  }

  /**
  * Renvoie une erreur si la command est imcomplète ou incorrecte.
  * @param {module:discord.js.Message} message
  * @param {string}                    command
  */
  private async commandError(message: Message, command: string){
   switch (command) {
     case undefined:
     this.man(this.manual, message);
     break;
     case "create":
     var embed: RichEmbed = new RichEmbed({
       title: `Erreur sur la commande ${process.env.COMMAND_NAME}${this.name} ${command}`,
       description: `
       Il manque un ou plusieur arguments ${process.env.COMMAND_NAME}${this.name} ${command} !
       Essayer ${process.env.COMMAND_NAME}${this.name} ${command} [name] [color: optional] ou faite '${process.env.COMMAND_NAME}${this.name} man' pour plus informations !
       `,
       color: 0xFF0000
     });
     return await this.replyDM(embed, message);
     break;
     case 'delete':
     var embed: RichEmbed = new RichEmbed({
       title: `Erreur sur la commande ${process.env.COMMAND_NAME}${this.name} ${command}`,
       description: `
       Il manque un ou plusieur arguments ${process.env.COMMAND_NAME}${this.name} ${command} !
       Essayer ${process.env.COMMAND_NAME}${this.name} ${command} [name] ou faite '${process.env.COMMAND_NAME}${this.name} man' pour plus informations !
       `,
       color: 0xFF0000
     });
     return await this.replyDM(embed, message);
     break;
     default:
     var embed: RichEmbed = new RichEmbed({
       title: `L'argument ${command} n'existe pas`,
       description: `
       La commande ${process.env.COMMAND_NAME}${this.name} ne possède pas l'argument ${command} ou faite '${process.env.COMMAND_NAME}${this.name} man' pour plus informations !
       `,
       color: 0xFF0000
     });
     return await this.replyDM(embed, message);
     break;
   }
  }

  /**
  * Créer un nouveau Role
  * @param {module:discord.js.RoleData} option
  * @param {module:discord.js.Message}  message
  */
  private async createRole(option: RoleData, message: Message){
    let server = message.guild;
    if(server.roles.find("name", option.name)) return await this.replyDM('Ce role éxiste déjà !', message);

    server.createRole({
      name: option.name,
      color: option.color ? option.color : '#'+(Math.random()*0xFFFFFF<<0).toString(16),
      permissions: option.permissions
    })
      .then((role: any) => message.reply(`Created new role with name ${role.name} and color ${role.color}`))
      .catch(console.error);
  }

  /**
   * Supprime un Role
   * @param {string | Promise<any>}     name
   * @param {module:discord.js.Message} message
   */
  private removeRole(name: string | Promise<any>, message: Message){
   let guild = message.client.guilds.first();
   let role = guild.roles.find("name", name);
   role.delete(`Role ${name} was deteled !`)
       .then(deleted => message.reply(`Deleted role ${deleted.name}`))
       .catch(console.error);
  }
}
