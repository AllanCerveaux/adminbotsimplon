import { ICommand, IFilter } from './interfaces'
import {Client, GuildMember, Message, Role, Channel, RichEmbed} from 'discord.js'

export default class Bot {

  public commands: ICommand[] = []; // Liste les commandes à utiliser
  private filters: IFilter[] = []; // Liste les filtres à utiliser
  private apiKey: string; // Clef d'api
  private client: Client;
  private invites: any;
  private modos: string[]; // Liste des modérateurs
  private modoRole: Role;

  constructor (client: Client, apiKey: string = '') {
    this.apiKey = apiKey;
    this.client = client;
    this.invites = {};
    this.client.on('ready', () => {
      let roles = this.client.guilds.first().roles;

      this.modoRole = roles.find('name', 'Simplon');
      if(this.modoRole){
        this.modos = this.modoRole.members.map(member => member.id);
      }

      this.client.guilds.forEach(g => {
        g.fetchInvites()
        .then(guildInvites => {
          this.invites[g.id] = guildInvites;
        })
        .catch(err => console.error);
      });
    });

    this.client.on('guildMemberAdd', this.newMember.bind(this));
    this.client.on('message', this.onMessage.bind(this));
    this.client.on('messageUpdate', (_, newMessage: Message) => this.onMessage(newMessage));
  }

  /**
   * Ajoute une commande au bot
   * @param {ICommand} command
   * @returns {Bot}
   */
   addCommand (command: ICommand): Bot {
     this.commands.push(command);
     return this;
   }

  /**
   * Ajoute un filtre au bot
   * @param {IFilter} filter
   */
   addFilter (filter: IFilter): Bot {
     this.filters.push(filter);
     return this;
   }

  /**
   * Connecte le bot
   */
   async connect () {
     await this.client.login(this.apiKey);
     this.client.on('error', e => console.error(e.message));
     return;
   }

  /**
   * Un message a été envoyé
   * @param {module:discord.js.Message} message
   */
   private onMessage (message: Message) {
     return (this.client.user && message.author.id === this.client.user.id) ||
     (message.content.startsWith(process.env.COMMAND_NAME) && this.runCommand(message) !== false) ||
     (message.channel.type !== 'dm' && this.runFilters(message) !== false)
   }

   /**
    * Ajout d'un role à la connexion d'un nouveau membre
    * @param {modules:discord.js.GuildMember} newMember
    */
    private newMember(newMember: GuildMember){
      newMember.guild.fetchInvites()
      .then((guildInvites: any) => {
        const ei = this.invites[newMember.guild.id];
        this.invites[newMember.guild.id] = guildInvites;
        /**
         * @BUG : Le premier utilisateur qui rejoint n'a pas de Role
         */
         const invite = guildInvites.find((i: any) => ei.get(i.code).uses < i.uses);
         invite.channel.parent.permissionOverwrites.forEach((perm: Role) => {
           let role = invite.channel.parent.guild.roles.find('id', perm.id);
           if(role.name !== "@everyone"){
             newMember.addRole(role)
             .then((member) => {
               const welcomeChannel: any = member.guild.channels.find('name', 'welcome');
               if(!welcomeChannel) return;

               welcomeChannel.send(`Bienvenue à ${member.displayName} qui fait partie de la team ${member.hoistRole.name === "Simplon" ? `${member.hoistRole} en tant qu'admin`: `${member.hoistRole} en tant qu'aprennant·e`} !`);
              /**
               * @TODO Ajouter un message de bienvenue avec toutes les information patrique pour le discord et simplon en général.
               */
               member.sendMessage(`
Bienvenue à toi ${member.displayName} sur le discord Simplon Occitanie. 
Tu peux allez lire quelque instruction concernant le discord sur ce liens ${member.hoistRole.name === "Simplon" ? "https://github.com/AllanCerveaux/AdminBotDiscord_TS/blob/master/.github/instruction.md" : "Comming Soon"}.
Si tu veux voir les commandes disponnible **${process.env.COMMAND_NAME}help**.
https://gph.is/1eqZUvN`);
             })
             .catch(err => console.error(err));
           }
         })       
       })
      .catch(err => console.error);
    }

  /**
   * Trouve la commande à lancer pour le message
   * @param {module:discord.js.Message} message
   */
   private runCommand (message: Message) {
     let parts = message.content.split(' ');
     let commandName = parts[0].replace(process.env.COMMAND_NAME, '');
     let command: ICommand = this.commands.find(c => c.name === commandName);
     if (command === undefined) return false;
     if(!command.active) return false;
     if (command.admin) {
       if (this.modos.includes(message.author.id)) {
         return command.run(message, parts.slice(1));
       } else {
         return false;
       }
     }

     return command.run(message, parts.slice(1));
   }

  /**
   * Renvoie le message sur tous les filtres
   * @param {module:discord.js.Message} message
   * @returns {boolean}
   */
   private runFilters (message: Message): boolean {
     return this.filters.find(f => f.filter(message)) === undefined;
   }
}
