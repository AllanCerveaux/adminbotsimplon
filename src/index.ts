import * as dotenv from 'dotenv'
import { Client } from "discord.js"

import Bot from './Bot'
import Logger from './utils/Logger'

import InsultFilters from "./filters/InsultFilters"

import { HelpCommand, RoleCommand, ChannelCommand, PromoCommand } from './commands'

import Init from './tasks/Init';

dotenv.config()

const client = new Client()
const logger = new Logger(client)

/**
 * @BUG : encore quelque bug au niveau de la création de channels et de l'envoie de l'invitation
 */
// Init.connect(client);

const bot = new Bot(client, process.env.API_KEY)
bot
    .addFilter(new InsultFilters())
    .addCommand(new HelpCommand(bot.commands))
    .addCommand(new RoleCommand())
    .addCommand(new ChannelCommand())
    .addCommand(new PromoCommand())
    .connect()
    .catch(function (e: string) {
        console.error(e)
    })
