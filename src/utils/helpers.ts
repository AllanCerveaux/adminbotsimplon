import { Message } from "discord.js";
import {PermList} from '../interfaces';

/**
 * Envoi un Message privée ou sur le channel si les MP sont désactiver
 * @param {module:discord.js.Message} message
 * @param {string} content
 * @return {Promise<any>}
 */

export const senDMorReply = async (message: Message, content: string) => {
    return message.author
        .createDM()
        .then((channel) => channel.send(content))
        .catch(() => message.reply(content.split('\n')[0]))
}
