import { Client, TextChannel } from "discord.js";
import { ILogger } from "../interfaces";

export default class Logger implements ILogger {
    private channel: TextChannel

    constructor(client: Client){
        client.on('ready', () => {
            this.channel = client.guilds.first().channels.find(channel => channel.name === 'log') as TextChannel
            client.user.setActivity(`${process.env.COMMAND_NAME}help`, {type: "LISTENING", url:"https://github.com/AllanCerveaux/AdminBotDiscord_TS"})
                .then(presence => console.log(`Activity set to ${presence.game ? presence.game.name : 'none'}`))
                .catch(console.error)
        })
        client.on('error',e => console.error(e.message))
    }

    async log(message: string){
        if(this.channel){
           return await this.channel.send(message)
        }
        return
    }
}