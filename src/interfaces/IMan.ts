export default interface IMan {
    readonly title: string,
    readonly description: string,
    readonly color: number,
    readonly footer: {
        text: string
    }
    readonly author: {
        name: string,
        icon: string,
        url: string
    }
}