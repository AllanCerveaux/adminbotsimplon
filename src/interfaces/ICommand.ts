import IMan from './IMan';
import { Message } from 'discord.js';

export default interface ICommand {
    readonly name: string
    readonly description: string
    readonly admin?: boolean
    readonly manual?: IMan
    readonly active: boolean

    run(msg: Message, args: string[]): any
}