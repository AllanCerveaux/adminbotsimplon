import { Message } from 'discord.js'

import PermList from "./PermList"
import ICommand from "./ICommand"
import IFilter from "./IFilter"
import ILogger from "./ILogger"
import IMan from "./IMan"

export {
    PermList,
    ICommand,
    IFilter,
    ILogger,
    IMan
}
