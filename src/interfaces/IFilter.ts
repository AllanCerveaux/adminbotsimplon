import { Message } from 'discord.js'

export default interface IFilter {
    filter(msg: Message): boolean
}