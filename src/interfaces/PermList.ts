export default interface PermList {
    readonly name: string
    readonly description: string
}