import {Client, Message, Channel, GuildChannel, CategoryChannel, Guild, Role, RichEmbed, PermissionResolvable} from "discord.js";

interface DefaultChannel {
	name: string
	topic: string
}

export default class Init {
	static client: Client;

	static publicChannel: DefaultChannel[] = [
		{name: 'welcome', topic: ':handshake: Bienvenue au nouveaux simploniens'},
		{name: 'général', topic: ':books: Salon pour passer des information important (event, meetup...) #nospam'},
		{name: ' la tarverne - veilles', topic: ':beers: Territoire du tavernier @LaurentDev , mais vous pouvez publier de la veilles tech '},
		{name: 'corrige-mon-code', topic: ':sos: Un soucie avec ton code, pose le ici on y jettera un coups d\'oeil'},
		{name: 'palons-peu-parlons-tech', topic: ':computer: On est ici pour parler tech sans modération !'},
		{name: ' trouve-ton-job', topic: ':woman_office_worker: Partager des offres d\'emploie par ici !'},
		{name: 'random', topic: ':rofl: Un petit salon pour échanger sur d\'autres sujets (toujours dans le respect de l\'esprit Simplon, bien entendu).'},
		{name: 'codeingame', topic: ':warning: RDV tous les vendredi à 15h (les RDV ne sont pas fixe) pour s\'arracher les cheveux !'},
		{name: 'codewars', topic: ':game_die: RDV journalier à 9h30 pour un kata en javascript !'},
	];

	static adminChannel: DefaultChannel[] = [
		{name: 'général', topic: 'Toutes les informations utiles pour les promo'},
		{name: 'nouvel-arrivant', topic: 'Channel d\'acceuil pour les nouveaux formateur·trice'},
		{name: 'organisation-event', topic: 'Hackthon, meetup, rencontre-intepromos...'},
		{name: 'jai-quelque-chose-à-dire', topic: 'Partager vos retour d\'expériences, vos projets ou même de vos problématique au sein des promo (ou pas).'},
		{name: 'techique-recherche-emploie', topic: 'Besoin ou Partager de conseil/bonne pratique sur de la TRE au sein des promo'},
		{name: 'projet-pédagogique', topic: 'Si vous avez trop ou pas assez de projets pédagogique hésiter pas à nous le faire savoir !'},
		{name: 'certfications', topic: 'Besoin ou Partager de conseil/bonne pratique pour la certification de votre référentiel'},
		{name: 'bug-et-retour', topic: 'Faites vos retour (bug, faute d\'orthographe...) du Bot '},
		{name: 'bot-command', topic: 'Lancer vos commande pour le bot ici'},
	];

	static admin_perm: PermissionResolvable = ["KICK_MEMBERS", "PRIORITY_SPEAKER", "MANAGE_WEBHOOKS", "MANAGE_MESSAGES", "MANAGE_NICKNAMES", "MANAGE_EMOJIS", "VIEW_CHANNEL", "READ_MESSAGES", "SEND_MESSAGES", "READ_MESSAGES", "READ_MESSAGE_HISTORY", "ADD_REACTIONS", "CONNECT", "SPEAK", "EMBED_LINKS"];

	/**
	 * Créer les channels/role public et admin
	 * @param {module:discord.js.Client} client
	 */
	static connect(client: Client){
		this.client = client;
		this.client.on('ready', () => {
			const server = client.guilds.first();
			this.makeChannel(server);
		})
	}

	/**
	 * Créer les channel public et admin.
	 * @param {module:discord.js.Message} message
	 */
	static async makeChannel(server: Guild){
		await this.makePublicCategory(server);
		await this.makeAdminCategory(server);
	}

	/**
	 * Créer la Catégory public
	 * @param {module:discord.js.Guild} server
	 */
	static async makePublicCategory(server: Guild){
		if(!server.channels.find('name', 'public')){
			await server.createChannel('public', 'category')
			.then((channel: CategoryChannel) => (
					channel.setPosition(0),
					this.makePublicChannel(channel, server)
				)
			)
			.catch(err => console.error(err));
		}
	}

	/**
	 * Créer les channels public
	 * @param {module:discord.js.CategoryChannel} parent
	 * @param {module:discord.js.Guild}           server
	 */
	static async makePublicChannel(parent: CategoryChannel, server: Guild){
		await this.publicChannel.forEach((channel: DefaultChannel) => {
			server.createChannel(channel.name, 'text')
			.then(child => {
				child.setParent(parent.id)
				.then(() => child.setTopic(channel.topic))
				.catch(err => console.error(err));
			});
		})
	}

	/**
	 * Créer la catégory Simplon Admin
	 * @param {module:discord.js.Guild}   server
	 * @param {module:discord.js.Message} message
	 */
	static async makeAdminCategory(server: Guild){
		if(!server.channels.find('name', 'Simplon Admin')){
			await server.createChannel('Simplon Admin', 'category')
			.then((channel: CategoryChannel) => (
					channel.setPosition(0),
					this.overwriteEveryonePermission(server, channel),
					this.createAdminRole(server, channel),
					this.makeAdminChannel(channel, server)
				)
			)
			.catch(err => console.error(err));
		}
	}

		/**
	 * Définit les permission du role @everyone sur un channel
	 * @param {module:discord.js.Guild} 			 server
	 * @param {module:discord.js.GuildChannel} channel
	 */
	static async overwriteEveryonePermission(server: Guild, channel: GuildChannel){
		await channel.overwritePermissions(server.roles.find('name', '@everyone'), {
			'ADD_REACTIONS': false,
			'READ_MESSAGES': false,                  'SEND_MESSAGES': false,
			'SEND_TTS_MESSAGES': false,              'MANAGE_MESSAGES': false,
			'EMBED_LINKS': false,                    'ATTACH_FILES': false,
			'READ_MESSAGE_HISTORY': false,           'MENTION_EVERYONE': false,
			'EXTERNAL_EMOJIS': false,                'CONNECT': false,
			'SPEAK': false
		});
	}

		/**
	 * Créer le role Simplon
	 * @param {module:discord.js.Guild} 			 server
	 * @param {module:discord.js.GuildChannel} channel
	 * @param {module:discord.js.Promo} 			 promo
	 */
	static async createAdminRole(server: Guild, channel: GuildChannel){
		if(!server.roles.find('name', 'Simplon')){
			await server.createRole({
				name: 'Simplon',
				color: 0xce0033,
				permissions: this.admin_perm,
				mentionable: true,
				hoist: true
			})
			.then((role: Role) => this.overwriteAdminPermission(server, channel))
			.catch((err: any) => console.error(err));
		}
	}

		/**
	 * Définit les permission pour le role pour les admins
	 * @param {module:discord.js.Guild} 		   server
	 * @param {module:discord.js.GuildChannel} channel
	 * @param {module:discord.js.string} 			 role
	 */
	static async overwriteAdminPermission(server: Guild, channel: GuildChannel){
		await channel.overwritePermissions(server.roles.find('name', 'Simplon'), {
			'CREATE_INSTANT_INVITE' : false,        'ADD_REACTIONS': true,
			'READ_MESSAGES': true,                  'SEND_MESSAGES': true,
			'SEND_TTS_MESSAGES': true,              'MANAGE_MESSAGES': true,
			'EMBED_LINKS': true,                    'ATTACH_FILES': true,
			'READ_MESSAGE_HISTORY': true,           'MENTION_EVERYONE': true,
			'EXTERNAL_EMOJIS': true,                'CONNECT': true,
			'SPEAK': true
		});
	}

	/**
	 * Créer les channels pour les admin
	 * @param {module:discord.js.CategoryChannel} parent
	 * @param {module:discord.js.Guild}           server
	 * @param {module:discord.js.Message}         message
	 */
	static async makeAdminChannel(parent: CategoryChannel, server: Guild){
		await this.adminChannel.forEach((channel: DefaultChannel) => {
			server.createChannel(channel.name, 'text')
			.then(child => {
				child.setParent(parent.id)
				.then(() => (
            	child.setTopic(channel.topic),
            	this.overwriteEveryonePermission(server, child),
            	this.overwriteAdminPermission(server, child),
				      child.name === "général" ? this.createAdminInstantInvite(parent) : null
            )
          )
				.catch(err => console.error(err));
			});
		})
	}

	/**
	 * Créer le code d'invitation
	 * @param {Message}         message
	 * @param {CategoryChannel} parent
	 */
	static async createAdminInstantInvite(parent: CategoryChannel){
			let invitChannel = parent.children.find("name", "général");
			await invitChannel.createInvite({
				temporary: false,
				maxAge: 0,
				unique: true
			})
			.then((invite: any) => {
				parent.children.find("name", "nouvel-arrivant")
					//@ts-ignore
					.send(`Utiliser ce code d'invitation pour les salariée simplon : **${invite.code}**`);
			})
			.catch((err: any) => console.error);
	}
}
