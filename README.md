# Admin Bot Discord TypeScript
___

>Code utilisé pour gérer le bot sur le channel discord de Simplon Occitanie.

**Sommaire :**

- [Getting Started](#getting-started)
- [Installation & Lancement](#hourglass_flowing_sand-installation--lancement)
- [Configurer le projet](#building_construction-configurer-le-projet)
- [How to ?](#how-to-)
    + [Ajouter une nouvelle commande](#ajouter-une-nouvelle-commande-au-bot)
- [Les instruction](https://gitlab.com/AllanCerveaux/adminbotsimplon/blob/master/.github/instruction.md)

> liens d'inviation du bot : https://discordapp.com/oauth2/authorize?client_id=[INSERT_CLIENT_ID_HERE]&scope=bot&permissions=8

### Quelques petit soucie :

Quand on créer une nouvelle promo avec la commande `promo` le bot génère un code d'invitation.
Ce code on peut l'utiliser 16 fois (on pourra mettre plus d'utilisation si le faut).
J'ai définit ce nombre car la première personne qui va rejoindre n'aura pas de role prévue avec le code d'invitation, pour contourner ce problème soit la première personne rejoint le server quitte celui-ci et rejoint de nouveau le serveur ou vous assigner manuellement sont rôle dès qu'elle rejoint le serveur.

### Getting Started

:raised_hand_with_fingers_splayed: **Requirements** :

Node :

- NodeJS >= 10.X
- NPM >= 6.X
- TSNODE >= 8.X
- TSC >= 3.X
- Nodemon >= 1.X
___

### :hourglass_flowing_sand: Installation & Lancement:

```bash
npm install
npm start # Production : start server.
npm run watch # Development : watch all ts file and reload server.
npm run build # build all ts file.
```

___

### :building_construction: Configurer le projet :

 - Dupliquer `.env.example`
```dotenv
API_KEY=[YOUR_BOT_SECRET_DISCORD]
COMMAND_NAME=[YOUR_COMMAND_NAME]
```

And let's code.

### How to ?

#### Ajouter une nouvelle commande au bot

1. Création de la nouvelle commande:

dans le dossier `src/commands` on créer la command `YourCommand.ts`

```typescript
import Command from './Command';
import { ICommand, IMan } from '../interfaces';
import { Message } from 'discord.js';

export default class YourCommand extends Command implements ICommand
{
    public name: string = 'your command name';
    public description: string = 'little description for help command';
    public manual: IMan = {
        title: 'title for command manual',
        description: `
            **Usage**: command [option] [arguments]
            **example**: example usage command

            **Options** :
                - option 1                  | description.
                - option 2 [language]       | description.
        `,
        color: 0x2196f3,
        footer: {
            text : "You can contribute on project on https://github.com/AllanCerveaux/AdminBotDiscord_TS"
        },
        author: {
            name: "Your Name",
            icon: "Your Icon",
            url: "Your profile github | twitter ..."
        }
    };
    /**
     * Lance la commande
     * @param {Message}  message
     * @param {string[]} args    All argument after you're command name
     */
    public async run(message: Message, args: string[]){
        /**
         * example : !command man -> run function man()
         * man(IMan, Message) extends from Command.ts
         */
        if(args[0] === "man"){
            this.man(this.manual, message);
        }
        // add all subcommand
    }

    // Add all method you need for you're command
}

```

2. Importer la commande dans `src/command/index.ts`.

```typescript
import HelpCommand from './HelpCommand';
import YourCommand from './YourCommand'

export {
    HelpCommand,
    YourCommand
}

```

3. Ajouter la nouvelle commande dans `index.ts`

```typescript
// ...
import { HelpCommand, YourCommand } from './commands'
// ...

const client = new Client()
const logger = new Logger(client)

const bot = new Bot(client, process.env.API_KEY);
bot
    .addFilter(new InsultFilters())
    .addCommand(new HelpCommand(bot.commands))
    .addCommand(new YourCommand())
    .connect()
    .catch(function (e: string) {
        console.error(e)
    });

```

#### Your create new Command for you're Bot !
![proud of you](https://media.giphy.com/media/fdyZ3qI0GVZC0/giphy.gif)
