# Bot Admin Disord Documentation

## Sommaire
- [Introduction](#introduction)
- [Commandes](#les-commandes)

## Introduction

Ce bot à pour but de gérer le Discord de Simplon Occitanie.
Avec différentes commandes pour créer des channels pour les promotions.

Le but et d'automatiser beaucoups de tâches chronophages (création channel pour les promo, attributions des rôles...).

___

## Les Commandes
Les différentes commandes:

- [Help](#help)
- [Promo](#promo)
- [Channel](#channel)
- [Role](#role)

### Help

**Description :**

La commande `help` permet d'obtenir la liste des commandes disponible en fonction de son rôle.

**Utilisation :**

command : `help` | renvoie la liste des commande disponible.

### Promo

**Description :**

La commande `promo` permet de créer/supprimer une nouvelle promotion sur discord avec :
- Catégory qui porte le nom de la promo. ex : `Simplon Toulouse DWWWM 1`
- Des channels par defaults pour la promotions.
    -  général, activités, veilles, détentes, musique, titre-pro, insertion-pro.
- Créer un rôle pour la promotion. ex: `Toulouse DWWM 1`
- Créer un liens d'invitations qui sera envoyer en message priver.
- Ajoute le rôle de la promotion à l'utilisateur qui lance la commande.

> Ces channels sont priver seul les personnes possédant le role de la promotion peuvent y accéder.
> Bug: Il faut attendre un moment avant d'envoyer le code d'invitation car sinon le premier apprennant qui rejoint n'aura pas de rôle.

**Utilisation :**
* commande : `promo`
* options :
    - `man`          | renvoie le manuel de la command
    - `create [promo_name] [promo_référentiel] [promo_id]` : `!promo create Toulouse DWWM 1` | créer un nouvelle promotion
    - `delete [promo_name] [promo_référentiel] [promo_id]` : `!promo delete Toulouse DWWM 1` | supprime une promotion

### Channel

**Description :**

La commande `channel` permet créer/supprimer un channel.

**Utilisation :**

* commande : `channel`
* options :
    - `man`                                             | renvoie le manuel de la command
    - `create [name] [type]` : `!channel veilles text`  | créer un nouveaux channel
    - `delete [name]` : `!channel delete veilles`       | supprime un channel


### Role

**Description :**

La commande `role` permet lister/créer/supprimer un role.

**Utilisation :**

* commande : `role`
* options :
    - `man`                              | renvoie le manuel de la command
    - `list`                             | list tous les roles du serveur
    - `create [name] [color:optional] [perms:optional]` : `!role Toulouse #FFF000 manage_channels,EMBED_LINKS` | Créer un role, les couleurs hexa sont pris en charge, vous pouvez retrouver la [liste des Permissions ici](https://github.com/meew0/discord-api-docs-1/blob/master/docs/topics/PERMISSIONS.md.)
    - `delete [role]` : `!role delete Toulouse` | Supprime un role.
